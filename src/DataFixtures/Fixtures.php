<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Fixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $task1 = new Task();
        $task1->setUsername('Orest');
        $task1->setEmail('orest@gmail.com');
        $task1->setDescription('Call Jane at 3:00 p.m.');
        $task2 = new Task();
        $task2->setUsername('Orest');
        $task2->setEmail('orest@gmail.com');
        $task2->setDescription('Get bored book from Tom');
        $task3 = new Task();
        $task3->setUsername('Thomas');
        $task3->setEmail('toms@gmail.com');
        $task3->setDescription('Go for a walk tonight');
        $task4 = new Task();
        $task4->setUsername('Jonathan');
        $task4->setEmail('jonath@gmail.com');
        $task4->setDescription('Go shopping at 6:30 p.m.');
        $task4->setCompleted(true);
        $user = new User();
        $user->setUsername('admin');
        $user->setPassword($this->passwordEncoder->encodePassword($user, '123'));
        $manager->persist($user);
        $manager->persist($task1);
        $manager->persist($task2);
        $manager->persist($task3);
        $manager->persist($task4);
        $manager->flush();
    }
}
